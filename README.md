# [Arrivalist Apple Stock Challenge](https://arrivalist-stock-app.herokuapp.com/)
Arrivalist Web Application Development Assignment 
------------------------------------------------
[x] 1) Create a web application that downloads daily Apple Stock prices from AWS S3 and displays the stock information in some JavaScript charting library (D3.js, HighCharts etc.)
  * Deployed application: https://arrivalist-stock-app.herokuapp.com/

[x] 2) In the given time period, which date would we have had to buy the stock and sell the stock in order to make the most profit? The application should mark these days and display the profit. 
  * Buy on August 21st 2018 for $214
  * Sell on October 2nd 2018 for $230
  * Maximum Profit Regular Trade: $16


[x] 3) What do you propose as solution if there are multiple time periods resulting in the same maximum profit?
  * The approach I took in finding the best buy/sell pair was a greedy approach which grabbed the first possible max profit pair. 

[x] 4) Bonus: if we are allowed to short the stock (borrow the stock, sell first then buy back back and give it back) what would be the most amount we could profit?
  * Sell on October 2nd 2018 for $230
  * Buy on November 5th 2018 for $201
  * Maximum Profit Shorting: $29

Sample file:  https://s3.amazonaws.com/arrivalist-puzzles/apple.csv

Deployed application: https://arrivalist-stock-app.herokuapp.com/

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

TODO:
Hit a real api endpoint for data
Refactor out maxProfit calculations away from useEffect. Was playing around with it.
Memoize maxProfit functions

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
