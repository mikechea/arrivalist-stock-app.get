import React, { useState, useEffect } from 'react';
import './App.css';
import Papa from 'papaparse'
import CanvasJSReact from './canvasjs.react';
import moment from 'moment'
import { findMaxProfit, shortStock } from './helpers'

function App() {
  const [options, setOptions] = useState({})

  useEffect(() => {
    Papa.parse("https://cors-anywhere.herokuapp.com/https://s3.amazonaws.com/arrivalist-puzzles/apple.csv", {
      download: true,
      complete: (results) => {        
        const rawCSVData = results.data.slice(1)

        const dataResult = rawCSVData.map((da, index) => {
          return { x: new Date(da[0]), y: parseInt(da[1]) } 
        })
    
        let maxProfit = findMaxProfit(dataResult)
    
        const { minIndex, maxIndex, buy, sell } = maxProfit
        dataResult[minIndex] = buy
        dataResult[maxIndex] = sell
    
        setOptions(
          {
            title:{
              text: "Apple stocks"
            },
            maxProfit: maxProfit,
            animationEnabled: true,
            theme: "dark2",
            axisY: {
              title: "US Dollars",
              includeZero: false,
              prefix: "$"
            },
            axisX: {
              title: "Date",
              interval: 1
            },
             data: [
              {
                type: "line",
                dataPoints: dataResult
              }
            ]
          }
        )
      }
    })
  }, []);

  const [purchaseOption, setPurchaseOption] = useState('Regular Trade')
  useEffect(() => {
    if(!options.data){ return }
    // resets old points
    let { dataPoints } = options.data[0]

    const { maxProfit } = options
    dataPoints[maxProfit.minIndex] = { x: maxProfit.buy.x, y: maxProfit.buy.y }
    dataPoints[maxProfit.maxIndex] = { x: maxProfit.sell.x, y:maxProfit.sell.y }

    // calulates new max values
    let newMaxProfit = purchaseOption === 'Regular Trade'
    ? findMaxProfit(options.data[0].dataPoints)
    : shortStock(options.data[0].dataPoints)
    
    const { minIndex, maxIndex, buy, sell } = newMaxProfit

    // reassigns data points
    dataPoints[minIndex] = buy
    dataPoints[maxIndex] = sell

    setOptions(prevOptions => ({
      ...prevOptions,
      maxProfit: newMaxProfit,
      data: [{
        type: "line",
        dataPoints: dataPoints
      }]
    }))
  }, [purchaseOption])

  function renderActions () {
    const { maxProfit } = options

    return(
      [<h2 key='buyDate'>Buy on {moment(maxProfit.buy.x).format('MMMM Do YYYY')} for ${maxProfit.buy.y}</h2>,
      <h2 key='sellDate'>Sell on {moment(maxProfit.sell.x).format('MMMM Do YYYY')} for ${maxProfit.sell.y}</h2>]
    )
  }
  
  const { maxProfit } = options
  const shouldRenderChart = maxProfit

  return (
    <div className="App">
      {
        shouldRenderChart && (
          <div>
            <h1>Maximum Profit {purchaseOption}: ${maxProfit.maxProfitAmount}</h1>
            <select onChange={(evt) =>setPurchaseOption(evt.target.value) }>
              <option value="Regular Trade">Regular Trade</option>
              <option value="Shorting">Shorting</option>
            </select>
            {
              purchaseOption === 'Regular Trade' ? renderActions() : renderActions().reverse()
            }
            <CanvasJSReact.CanvasJSChart options={options} />
          </div>
        )
      }
    </div>
  );
}

export default App;
