export function findMaxProfit (data, balance) {
  let minPrice = data[0].y

  let minIndex = 0
  let currentMin = 0
  let maxIndex = 1
  
  data.forEach((da, index) => {
    const { y: value } = da

    if(value < minPrice) { 
      minPrice = value
      currentMin = index;
    }
    
    // new best profit
    if(data[maxIndex].y - data[minIndex].y < value - data[currentMin].y) {
      maxIndex = index;
      minIndex = currentMin;
    }
  })
  const maxProfitAmount = Math.abs(data[maxIndex].y - data[minIndex].y)

  data[minIndex] = { ...data[minIndex], indexLabel: "buy/low", markerType: "triangle",markerColor: "red", markerSize: 12 }
  data[maxIndex] = { ...data[maxIndex], indexLabel: "sell/high", markerType: "cross",markerColor: "red", markerSize: 12 }

  return { minIndex: minIndex, maxIndex: maxIndex, buy: data[minIndex],
    sell: data[maxIndex],
    maxProfitAmount: maxProfitAmount }
}

export function shortStock (data) {
  let maxPrice = data[0].y

  let maxIndex = 0
  let currentMax = 0
  let minIndex = 1
  
  data.forEach((da, index) => {
    const { y: value } = da

    if(value > maxPrice) { 
      maxPrice = value
      currentMax = index;
    }
    
    // new best profit
    if(data[maxIndex].y - data[minIndex].y < data[currentMax].y - value) {
      minIndex = index;
      maxIndex = currentMax;
    }
  })

  data[minIndex] = { ...data[minIndex], indexLabel: "buy/low", markerType: "triangle",markerColor: "red", markerSize: 12 }
  data[maxIndex] = { ...data[maxIndex], indexLabel: "sell/high", markerType: "cross",markerColor: "red", markerSize: 12 }

  const maxProfitAmount = Math.abs(data[maxIndex].y - data[minIndex].y)

  return { minIndex: minIndex, maxIndex: maxIndex, buy: data[minIndex],
    sell: data[maxIndex],
    maxProfitAmount: maxProfitAmount }
}